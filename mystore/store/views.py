from django.shortcuts import redirect
from django.views import generic
from rest_framework import viewsets, permissions

from store.forms import ContactForm
from store.models import Product
from store.serializers import ProductSerializer


class ContactView(generic.FormView):
    form_class = ContactForm
    template_name = "store/contact.html"

    def form_valid(self, form):
        return redirect("/contact/?success=1")


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]
