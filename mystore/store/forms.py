from crispy_forms.helper import FormHelper
from django import forms


class ContactForm(forms.Form):
    sender = forms.CharField(label="Expéditeur")
    subject = forms.CharField(label="Objet")
    message = forms.CharField(label="Message", widget=forms.Textarea)

    helper = FormHelper()
    helper.form_tag = False
