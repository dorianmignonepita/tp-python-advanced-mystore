from django.urls import path, include
from rest_framework import routers

from store import views

router = routers.DefaultRouter()
router.register(r'products', views.ProductViewSet)

urlpatterns = [
    path('contact/', views.ContactView.as_view()),

    path('api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/', include(router.urls)),
]
