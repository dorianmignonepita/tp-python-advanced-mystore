import Levenshtein
from django.core.exceptions import ValidationError
from django.db import models


class Warehouse(models.Model):
    name = models.CharField(max_length=255)


class Product(models.Model):
    label = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    price = models.FloatField()
    available_qty = models.IntegerField(default=0)
    has_quantity = models.BooleanField(default=True)

    warehouse = models.ForeignKey(Warehouse, on_delete=models.SET_NULL, null=True, blank=True)

    @property
    def is_available(self):
        return not self.has_quantity or self.available_qty > 0

    @property
    def stock_status(self):
        if self.has_quantity and self.available_qty > 0:
            return "En stock"

        if not self.has_quantity:
            return "Illimité"

        return "Indisponible"

    def sell_single(self):
        if self.has_quantity and self.available_qty <= 0:
            return False

        self.available_qty -= 1
        self.save()
        return True

    @property
    def short_description(self):
        return f"{self.description[:100]} ..."

    def get_similar(self):
        matches = Product.objects.filter(warehouse=self.warehouse).exclude(id=self.id)
        similarity_scores = [(Levenshtein.distance(self.label, p2.label), p2) for p2 in matches]
        similarity_scores.sort(key=lambda x: x[0])

        return [prod for _, prod in similarity_scores[:3]]

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    def clean(self):
        if self.price <= 0:
            raise ValidationError("Le prix doit être positif.")

        if not self.has_quantity and self.available_qty > 0:
            raise ValidationError("Ce produit ne peut pas être stocké.")

    class Meta:
        verbose_name = "Product"
