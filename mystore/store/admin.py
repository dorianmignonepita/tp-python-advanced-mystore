from django.contrib import admin

from store.models import Product, Warehouse


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass

@admin.register(Warehouse)
class WareHouseAdmin(admin.ModelAdmin):
    pass